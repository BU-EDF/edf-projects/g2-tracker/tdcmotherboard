#!/bin/bash
#
# make zip file and template README for this project
# generate gerbers with "use proper filename extensions" UNCHECKED
# so all the gerber file names end in ".pho"
#
base="G2-TDC-MB"
orig="TDC_Mother"
cp ${orig}-B_Cu.pho ${base}-layer_4.pho
cp ${orig}-B_Mask.pho ${base}-B_Mask.pho
cp ${orig}-B_Paste.pho ${base}-B_Paste.pho
cp ${orig}-B_SilkS.pho ${base}-B_SilkS.pho
cp ${orig}-Cmts_User.pho ${base}-Fab_Dwg.pho
cp ${orig}.drl ${base}.drl
cp ${orig}-drl_map.pho ${base}-drl_map.pho
cp ${orig}-F_Cu.pho ${base}-layer_1.pho
cp ${orig}-F_Mask.pho ${base}-F_Mask.pho
cp ${orig}-F_Paste.pho ${base}-F_Paste.pho
cp ${orig}-F_SilkS.pho ${base}-F_SilkS.pho
cp ${orig}-Ground.pho ${base}-layer_2.pho
cp ${orig}-Power.pho ${base}-layer_3.pho
cp ${orig}-NPTH.drl ${base}-NPTH.drl
cp ${orig}-NPTH-drl_map.pho ${base}-NPTH-drl_map.pho

