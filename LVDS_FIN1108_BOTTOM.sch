EESchema Schematic File Version 2
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:FIN1108
LIBS:ERM8-RA
LIBS:4816P-1-101LF
LIBS:LSHM-130-02.5-F-DV-N (TDC_Inputs)
LIBS:T1M_10_xx_S_RA_TR_(IO__Power)
LIBS:tps78630kttt
LIBS:tps79533
LIBS:30310-5002hb
LIBS:640456-2
LIBS:wire_connector
LIBS:FTSH-108-02-xx-DV (JTAG)
LIBS:jumper
LIBS:power
LIBS:+4
LIBS:TDC_Mother-cache
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 7
Title ""
Date "20 apr 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L LVDS_FIN1108 U5
U 1 1 544CF9BD
P 6550 3600
AR Path="/54603C99/544CF9BD" Ref="U5"  Part="1" 
AR Path="/546352F5/544CF9BD" Ref="U8"  Part="1" 
AR Path="/546358D0/544CF9BD" Ref="U11"  Part="1" 
AR Path="/54635FFB/544CF9BD" Ref="U14"  Part="1" 
F 0 "U8" H 5950 1950 60  0000 C CNN
F 1 "LVDS_FIN1108" V 6550 3600 79  0000 C CNN
F 2 "" H 6100 3200 60  0000 C CNN
F 3 "" H 6100 3200 60  0000 C CNN
F 4 "FIN1108MTDXTR-ND" H 6550 3600 60  0001 C CNN "Field1"
F 5 "Digi-Key" H 6550 3600 60  0001 C CNN "Field2"
	1    6550 3600
	1    0    0    -1  
$EndComp
Text HLabel 3450 2050 0    60   Input ~ 0
ASDQ_N[7..0]
Text HLabel 3450 1950 0    60   Input ~ 0
ASDQ_P[7..0]
Entry Wire Line
	9550 2200 9450 2300
Entry Wire Line
	9550 2800 9450 2900
Entry Wire Line
	9350 2700 9250 2800
Entry Wire Line
	9550 3400 9450 3500
Entry Wire Line
	9350 3300 9250 3400
Entry Wire Line
	9550 4000 9450 4100
Entry Wire Line
	9350 3900 9250 4000
Text HLabel 9750 1950 2    60   Input ~ 0
LVDS_P[7..0]
Text HLabel 9750 2050 2    60   Input ~ 0
LVDS_N[7..0]
NoConn ~ 5700 4700
$Comp
L +3.3V #PWR033
U 1 1 54B24748
P 6550 800
AR Path="/54603C99/54B24748" Ref="#PWR033"  Part="1" 
AR Path="/546352F5/54B24748" Ref="#PWR038"  Part="1" 
AR Path="/546358D0/54B24748" Ref="#PWR038"  Part="1" 
AR Path="/54635FFB/54B24748" Ref="#PWR043"  Part="1" 
F 0 "#PWR038" H 6550 760 30  0001 C CNN
F 1 "+3.3V" H 6550 910 30  0000 C CNN
F 2 "" H 6550 800 60  0000 C CNN
F 3 "" H 6550 800 60  0000 C CNN
	1    6550 800 
	1    0    0    -1  
$EndComp
Text Label 7450 2300 0    60   ~ 0
LVDS_P0
Text Label 7450 2500 0    60   ~ 0
LVDS_P1
Text Label 7450 2900 0    60   ~ 0
LVDS_P2
Text Label 7450 3100 0    60   ~ 0
LVDS_P3
Text Label 7450 3500 0    60   ~ 0
LVDS_P4
Text Label 7450 3700 0    60   ~ 0
LVDS_P5
Text Label 7450 4100 0    60   ~ 0
LVDS_P6
Text Label 7450 4300 0    60   ~ 0
LVDS_P7
Text Label 7450 2200 0    60   ~ 0
LVDS_N0
Text Label 7450 2600 0    60   ~ 0
LVDS_N1
Text Label 7450 2800 0    60   ~ 0
LVDS_N2
Text Label 7450 3200 0    60   ~ 0
LVDS_N3
Text Label 7450 3400 0    60   ~ 0
LVDS_N4
Text Label 7450 3800 0    60   ~ 0
LVDS_N5
Text Label 7450 4000 0    60   ~ 0
LVDS_N6
Text Label 7450 4400 0    60   ~ 0
LVDS_N7
Text Label 3900 2300 0    60   ~ 0
ASDQ_P0
Text Label 3900 2500 0    60   ~ 0
ASDQ_P1
Text Label 3900 2900 0    60   ~ 0
ASDQ_P2
Text Label 3900 3100 0    60   ~ 0
ASDQ_P3
Text Label 3900 3500 0    60   ~ 0
ASDQ_P4
Text Label 3900 3700 0    60   ~ 0
ASDQ_P5
Text Label 3900 4100 0    60   ~ 0
ASDQ_P6
Text Label 3900 4300 0    60   ~ 0
ASDQ_P7
Text Label 3900 2200 0    60   ~ 0
ASDQ_N0
Text Label 3900 2600 0    60   ~ 0
ASDQ_N1
Text Label 3900 2800 0    60   ~ 0
ASDQ_N2
Text Label 3900 3200 0    60   ~ 0
ASDQ_N3
Text Label 3900 3400 0    60   ~ 0
ASDQ_N4
Text Label 3900 3800 0    60   ~ 0
ASDQ_N5
Text Label 3900 4000 0    60   ~ 0
ASDQ_N6
Text Label 3900 4400 0    60   ~ 0
ASDQ_N7
$Comp
L GND #PWR034
U 1 1 5460C42A
P 7100 5800
AR Path="/54603C99/5460C42A" Ref="#PWR034"  Part="1" 
AR Path="/546352F5/5460C42A" Ref="#PWR039"  Part="1" 
AR Path="/546358D0/5460C42A" Ref="#PWR039"  Part="1" 
AR Path="/54635FFB/5460C42A" Ref="#PWR044"  Part="1" 
F 0 "#PWR039" H 7100 5800 30  0001 C CNN
F 1 "GND" H 7100 5730 30  0001 C CNN
F 2 "" H 7100 5800 60  0000 C CNN
F 3 "" H 7100 5800 60  0000 C CNN
	1    7100 5800
	1    0    0    -1  
$EndComp
$Comp
L +3.3V #PWR035
U 1 1 54B2474A
P 7500 4600
AR Path="/54603C99/54B2474A" Ref="#PWR035"  Part="1" 
AR Path="/546352F5/54B2474A" Ref="#PWR040"  Part="1" 
AR Path="/546358D0/54B2474A" Ref="#PWR040"  Part="1" 
AR Path="/54635FFB/54B2474A" Ref="#PWR045"  Part="1" 
F 0 "#PWR040" H 7500 4560 30  0001 C CNN
F 1 "+3.3V" H 7500 4710 30  0000 C CNN
F 2 "" H 7500 4600 60  0000 C CNN
F 3 "" H 7500 4600 60  0000 C CNN
	1    7500 4600
	1    0    0    -1  
$EndComp
$Comp
L C C10
U 1 1 5462AD4A
P 7050 1250
AR Path="/54603C99/5462AD4A" Ref="C10"  Part="1" 
AR Path="/546352F5/5462AD4A" Ref="C15"  Part="1" 
AR Path="/546358D0/5462AD4A" Ref="C20"  Part="1" 
AR Path="/54635FFB/5462AD4A" Ref="C25"  Part="1" 
F 0 "C15" V 7000 1350 50  0000 C CNN
F 1 ".01uF" V 7000 1100 50  0000 C CNN
F 2 "" H 7050 1250 60  0000 C CNN
F 3 "" H 7050 1250 60  0000 C CNN
F 4 "399-1091-1-ND" V 7050 1250 60  0001 C CNN "Field1"
F 5 "Digi-Key" V 7050 1250 60  0001 C CNN "Field2"
	1    7050 1250
	0    -1   -1   0   
$EndComp
$Comp
L C C11
U 1 1 5462AE4E
P 7050 1600
AR Path="/54603C99/5462AE4E" Ref="C11"  Part="1" 
AR Path="/546352F5/5462AE4E" Ref="C16"  Part="1" 
AR Path="/546358D0/5462AE4E" Ref="C21"  Part="1" 
AR Path="/54635FFB/5462AE4E" Ref="C26"  Part="1" 
F 0 "C16" V 7000 1700 50  0000 C CNN
F 1 ".01uF" V 7000 1450 50  0000 C CNN
F 2 "" H 7050 1600 60  0000 C CNN
F 3 "" H 7050 1600 60  0000 C CNN
F 4 "399-1091-1-ND" V 7050 1600 60  0001 C CNN "Field1"
F 5 "Digi-Key" V 7050 1600 60  0001 C CNN "Field2"
	1    7050 1600
	0    -1   -1   0   
$EndComp
$Comp
L C C9
U 1 1 5462AE54
P 6000 1600
AR Path="/54603C99/5462AE54" Ref="C9"  Part="1" 
AR Path="/546352F5/5462AE54" Ref="C14"  Part="1" 
AR Path="/546358D0/5462AE54" Ref="C19"  Part="1" 
AR Path="/54635FFB/5462AE54" Ref="C24"  Part="1" 
F 0 "C14" V 5950 1700 50  0000 C CNN
F 1 ".01uF" V 5950 1450 50  0000 C CNN
F 2 "" H 6000 1600 60  0000 C CNN
F 3 "" H 6000 1600 60  0000 C CNN
F 4 "399-1091-1-ND" V 6000 1600 60  0001 C CNN "Field1"
F 5 "Digi-Key" V 6000 1600 60  0001 C CNN "Field2"
	1    6000 1600
	0    -1   -1   0   
$EndComp
$Comp
L C C8
U 1 1 5462AE5A
P 6000 1250
AR Path="/54603C99/5462AE5A" Ref="C8"  Part="1" 
AR Path="/546352F5/5462AE5A" Ref="C13"  Part="1" 
AR Path="/546358D0/5462AE5A" Ref="C18"  Part="1" 
AR Path="/54635FFB/5462AE5A" Ref="C23"  Part="1" 
F 0 "C13" V 5950 1350 50  0000 C CNN
F 1 ".01uF" V 5950 1100 50  0000 C CNN
F 2 "" H 6000 1250 60  0000 C CNN
F 3 "" H 6000 1250 60  0000 C CNN
F 4 "399-1091-1-ND" V 6000 1250 60  0001 C CNN "Field1"
F 5 "Digi-Key" V 6000 1250 60  0001 C CNN "Field2"
	1    6000 1250
	0    -1   -1   0   
$EndComp
$Comp
L GND #PWR036
U 1 1 5462B475
P 7400 1850
AR Path="/54603C99/5462B475" Ref="#PWR036"  Part="1" 
AR Path="/546352F5/5462B475" Ref="#PWR041"  Part="1" 
AR Path="/546358D0/5462B475" Ref="#PWR041"  Part="1" 
AR Path="/54635FFB/5462B475" Ref="#PWR046"  Part="1" 
F 0 "#PWR041" H 7400 1850 30  0001 C CNN
F 1 "GND" H 7400 1780 30  0001 C CNN
F 2 "" H 7400 1850 60  0000 C CNN
F 3 "" H 7400 1850 60  0000 C CNN
	1    7400 1850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR037
U 1 1 5462B482
P 5650 1850
AR Path="/54603C99/5462B482" Ref="#PWR037"  Part="1" 
AR Path="/546352F5/5462B482" Ref="#PWR042"  Part="1" 
AR Path="/546358D0/5462B482" Ref="#PWR042"  Part="1" 
AR Path="/54635FFB/5462B482" Ref="#PWR047"  Part="1" 
F 0 "#PWR042" H 5650 1850 30  0001 C CNN
F 1 "GND" H 5650 1780 30  0001 C CNN
F 2 "" H 5650 1850 60  0000 C CNN
F 3 "" H 5650 1850 60  0000 C CNN
	1    5650 1850
	1    0    0    -1  
$EndComp
Wire Bus Line
	3450 2050 3650 2050
Wire Wire Line
	3900 2200 5700 2200
Wire Wire Line
	3750 2300 5700 2300
Wire Wire Line
	3750 2500 5700 2500
Wire Wire Line
	3900 2800 5700 2800
Wire Wire Line
	3750 2900 5700 2900
Wire Wire Line
	3750 3100 5700 3100
Wire Wire Line
	3900 3200 5700 3200
Wire Wire Line
	3900 3400 5700 3400
Wire Wire Line
	3750 3500 5700 3500
Wire Wire Line
	3750 3700 5700 3700
Wire Wire Line
	3900 3800 5700 3800
Wire Wire Line
	3900 4000 5700 4000
Wire Wire Line
	3750 4100 5700 4100
Wire Wire Line
	3900 4400 5700 4400
Wire Bus Line
	9750 2050 9550 2050
Wire Bus Line
	9750 1950 9350 1950
Wire Wire Line
	9250 2200 7400 2200
Wire Wire Line
	9450 2300 7400 2300
Wire Wire Line
	7400 2500 9450 2500
Wire Wire Line
	9250 2800 7400 2800
Wire Wire Line
	9450 2900 7400 2900
Wire Wire Line
	7400 3100 9450 3100
Wire Wire Line
	9250 3400 7400 3400
Wire Wire Line
	9450 3500 7400 3500
Wire Wire Line
	7400 3700 9450 3700
Wire Wire Line
	9250 4000 7400 4000
Wire Wire Line
	9450 4100 7400 4100
Wire Wire Line
	7400 4300 9450 4300
Wire Wire Line
	9250 4400 7400 4400
Wire Bus Line
	9350 1950 9350 4350
Wire Wire Line
	6350 5650 6350 5500
Wire Wire Line
	6750 5650 6750 5500
Connection ~ 6550 5650
Wire Wire Line
	6650 5650 6650 5500
Connection ~ 6650 5650
Wire Wire Line
	6450 5650 6450 5500
Connection ~ 6450 5650
Connection ~ 6350 5650
Wire Wire Line
	7500 5650 6350 5650
Wire Bus Line
	3800 1950 3800 4350
Wire Bus Line
	3800 1950 3450 1950
Wire Wire Line
	7400 4800 7500 4800
Wire Wire Line
	7500 4800 7500 5650
Wire Wire Line
	7400 4900 7500 4900
Connection ~ 7500 4900
Wire Wire Line
	7400 5000 7500 5000
Connection ~ 7500 5000
Wire Wire Line
	7400 5100 7500 5100
Connection ~ 7500 5100
Connection ~ 6750 5650
Wire Wire Line
	6550 5500 6550 5650
Connection ~ 7100 5650
Wire Wire Line
	7100 5650 7100 5800
Wire Wire Line
	6350 1700 6350 1600
Wire Wire Line
	6200 1600 6850 1600
Wire Wire Line
	6450 1700 6450 1250
Wire Wire Line
	6200 1250 6850 1250
Wire Wire Line
	6550 800  6550 1700
Wire Wire Line
	6550 900  6250 900 
Wire Wire Line
	6650 1250 6650 1700
Wire Wire Line
	6750 1600 6750 1700
Wire Wire Line
	5750 900  5650 900 
Wire Wire Line
	5650 900  5650 1850
Wire Wire Line
	5650 1250 5800 1250
Connection ~ 5650 1250
Wire Wire Line
	5650 1600 5800 1600
Connection ~ 5650 1600
Wire Wire Line
	7250 1250 7400 1250
Wire Wire Line
	7400 1250 7400 1850
Wire Wire Line
	7250 1600 7400 1600
Connection ~ 7400 1600
Connection ~ 6450 1250
Connection ~ 6550 1250
Connection ~ 6550 1600
Connection ~ 6350 1600
Connection ~ 6750 1600
Connection ~ 6650 1250
Connection ~ 6550 900 
$Comp
L 4816P-1-101LF U3
U 1 1 5480C3B8
P 5000 2600
AR Path="/54603C99/5480C3B8" Ref="U3"  Part="1" 
AR Path="/546352F5/5480C3B8" Ref="U6"  Part="1" 
AR Path="/546358D0/5480C3B8" Ref="U9"  Part="1" 
AR Path="/54635FFB/5480C3B8" Ref="U12"  Part="1" 
F 0 "U6" H 4750 3100 60  0000 C CNN
F 1 "4816P-1-101LF" H 5050 3200 60  0000 C CNN
F 2 "" H 5000 2600 60  0000 C CNN
F 3 "" H 5000 2600 60  0000 C CNN
F 4 "4816P-1-101LFTR-ND" H 5000 2600 60  0001 C CNN "Field1"
F 5 "Digi-Key" H 5000 2600 60  0001 C CNN "Field2"
	1    5000 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 4300 5700 4300
Wire Wire Line
	4400 2500 4400 2550
Connection ~ 4400 2500
Wire Wire Line
	5600 2600 5600 2550
Connection ~ 5600 2600
Wire Wire Line
	4400 3100 4400 3150
Connection ~ 4400 3100
Wire Wire Line
	4400 3700 4400 3750
Connection ~ 4400 3700
Wire Wire Line
	4400 4300 4400 4350
Connection ~ 4400 4300
Wire Wire Line
	5600 4400 5600 4350
Connection ~ 5600 4400
Wire Wire Line
	5600 3800 5600 3750
Connection ~ 5600 3800
Wire Wire Line
	5600 3200 5600 3150
Connection ~ 5600 3200
Entry Wire Line
	3650 2200 3750 2300
Entry Wire Line
	3650 2400 3750 2500
Entry Wire Line
	3650 2800 3750 2900
Entry Wire Line
	3650 3000 3750 3100
Entry Wire Line
	3650 3400 3750 3500
Entry Wire Line
	3650 3600 3750 3700
Entry Wire Line
	3650 4000 3750 4100
Entry Wire Line
	3650 4200 3750 4300
Entry Wire Line
	3800 2100 3900 2200
Entry Wire Line
	3800 2550 3900 2650
Wire Wire Line
	3900 2600 5700 2600
Wire Wire Line
	3900 2600 3900 2650
Entry Wire Line
	3800 2700 3900 2800
Entry Wire Line
	3800 3150 3900 3250
Entry Wire Line
	3800 3300 3900 3400
Entry Wire Line
	3800 3750 3900 3850
Entry Wire Line
	3800 3900 3900 4000
Wire Wire Line
	3900 3200 3900 3250
Wire Wire Line
	3900 3800 3900 3850
Entry Wire Line
	3800 4350 3900 4450
Wire Wire Line
	3900 4400 3900 4450
Wire Bus Line
	3650 2050 3650 4200
Entry Wire Line
	9250 2200 9350 2100
Entry Wire Line
	9450 2500 9550 2400
Wire Wire Line
	9250 2600 9250 2650
Wire Wire Line
	7400 2600 9250 2600
Entry Wire Line
	9250 2650 9350 2550
Entry Wire Line
	9450 3100 9550 3000
Entry Wire Line
	9250 3250 9350 3150
Wire Wire Line
	9250 3200 9250 3250
Wire Wire Line
	9250 3200 7400 3200
Entry Wire Line
	9450 3700 9550 3600
Entry Wire Line
	9250 3850 9350 3750
Wire Wire Line
	9250 3800 9250 3850
Entry Wire Line
	9450 4300 9550 4200
Wire Wire Line
	9250 4400 9250 4450
Entry Wire Line
	9250 4450 9350 4350
Wire Bus Line
	9550 2050 9550 4200
Wire Wire Line
	4400 4050 4400 4100
Connection ~ 4400 4100
Wire Wire Line
	4400 3450 4400 3500
Connection ~ 4400 3500
Wire Wire Line
	4400 2850 4400 2900
Connection ~ 4400 2900
Wire Wire Line
	4400 2250 4400 2300
Connection ~ 4400 2300
Wire Wire Line
	5600 2250 5600 2200
Connection ~ 5600 2200
Wire Wire Line
	5600 2850 5600 2800
Connection ~ 5600 2800
Wire Wire Line
	5600 3450 5600 3400
Connection ~ 5600 3400
Wire Wire Line
	5600 4050 5600 4000
Connection ~ 5600 4000
$Comp
L R_PACK4 RP1
U 1 1 5533ACEB
P 8500 2000
AR Path="/54603C99/5533ACEB" Ref="RP1"  Part="1" 
AR Path="/546352F5/5533ACEB" Ref="RP3"  Part="1" 
F 0 "RP3" H 8500 2450 40  0000 C CNN
F 1 "R_PACK4" H 8500 1950 40  0000 C CNN
F 2 "~" H 8500 2000 60  0000 C CNN
F 3 "~" H 8500 2000 60  0000 C CNN
F 4 "CAT16-101J4LF" H 8500 2000 60  0001 C CNN "Field1"
F 5 "DigiKey" H 8500 2000 60  0001 C CNN "Field2"
	1    8500 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	8300 1850 8250 1850
Wire Wire Line
	8700 1850 8750 1850
Wire Wire Line
	8700 1750 8800 1750
Wire Wire Line
	8700 1650 8850 1650
$Comp
L R_PACK4 RP2
U 1 1 5533B271
P 8550 4950
AR Path="/54603C99/5533B271" Ref="RP2"  Part="1" 
AR Path="/546352F5/5533B271" Ref="RP4"  Part="1" 
F 0 "RP4" H 8550 5400 40  0000 C CNN
F 1 "R_PACK4" H 8550 4900 40  0000 C CNN
F 2 "~" H 8550 4950 60  0000 C CNN
F 3 "~" H 8550 4950 60  0000 C CNN
F 4 "CAT16-101J4LF" H 8550 4950 60  0001 C CNN "Field1"
F 5 "DigiKey" H 8550 4950 60  0001 C CNN "Field2"
	1    8550 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 4600 7500 4700
Connection ~ 7500 4700
Wire Wire Line
	9250 3800 7400 3800
Wire Wire Line
	8300 1750 8200 1750
Wire Wire Line
	8300 1650 8150 1650
Wire Wire Line
	8150 1650 8150 2300
Connection ~ 8150 2300
Wire Wire Line
	8200 1750 8200 2500
Connection ~ 8200 2500
Wire Wire Line
	8250 1850 8250 2900
Connection ~ 8250 2900
Wire Wire Line
	8300 1950 8300 3100
Connection ~ 8300 3100
Wire Wire Line
	8850 1650 8850 2200
Connection ~ 8850 2200
Wire Wire Line
	8800 1750 8800 2600
Connection ~ 8800 2600
Wire Wire Line
	8750 1850 8750 2800
Connection ~ 8750 2800
Wire Wire Line
	8700 1950 8700 3200
Connection ~ 8700 3200
Wire Wire Line
	8350 4600 8350 3500
Connection ~ 8350 3500
Wire Wire Line
	8350 4700 8300 4700
Wire Wire Line
	8300 4700 8300 3700
Connection ~ 8300 3700
Wire Wire Line
	8350 4800 8250 4800
Wire Wire Line
	8250 4800 8250 4100
Connection ~ 8250 4100
Wire Wire Line
	8350 4900 8200 4900
Wire Wire Line
	8200 4900 8200 4300
Connection ~ 8200 4300
Wire Wire Line
	8750 4600 8750 3400
Connection ~ 8750 3400
Wire Wire Line
	8750 4700 8800 4700
Wire Wire Line
	8800 4700 8800 3800
Connection ~ 8800 3800
Wire Wire Line
	8750 4800 8850 4800
Wire Wire Line
	8850 4800 8850 4000
Connection ~ 8850 4000
Wire Wire Line
	8750 4900 8900 4900
Wire Wire Line
	8900 4900 8900 4400
Connection ~ 8900 4400
Wire Wire Line
	7500 4700 7400 4700
$EndSCHEMATC
