
HOWTO generate gerbers:

Check only options:
  "Plot module value on silkscreen"
  "Plot module reference on silkscreen"
  "Plot other module texts on silkscreen"

Check layers:
  F.Cu,  B.Cu
  Power,  Ground
  F.Paste, F.Silks, F.Mask
  B.Paste, B.Silks, B.Mask
  Cmts.User

Run the generate gerbers, generate drills
and generate drill maps.

Go to gerber directory, run make_fab.sh

zip -r G2-TDC-MB-RevX.zip G2-TDC-MB* README.txt

