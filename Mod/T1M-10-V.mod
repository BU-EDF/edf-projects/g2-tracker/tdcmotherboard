PCBNEW-LibModule-V1  Fri 21 Nov 2014 11:48:39 AM EST
# encoding utf-8
Units mm
$INDEX
T1M-10-V
$EndINDEX
$MODULE T1M-10-V
Po 0 0 0 15 546F6A29 00000000 ~~
Li T1M-10-V
Sc 0
AR 
Op 0 0 0
T0 0 -2.76 1 1 0 0.15 N V 21 N "T1M-10-V"
T1 0 3.9 1 1 0 0.15 N V 21 N "VAL**"
DS -5 -1.725 5 -1.725 0.15 21
DS -6 1 -6 0.25 0.15 21
DS 6 1 6 0.25 0.15 21
$PAD
Sh "" R 0.8 1.8 0 0 0
Dr 0 0 0
At HOLE N 00E0FFFF
Ne 0 ""
Po -5.8 -1.025
$EndPAD
$PAD
Sh "1" R 0.6 1.55 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4.5 1.5
.LocalClearance 0.07
$EndPAD
$PAD
Sh "2" R 0.6 1.55 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -3.5 1.5
.LocalClearance 0.07
$EndPAD
$PAD
Sh "3" R 0.6 1.55 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2.5 1.5
.LocalClearance 0.07
$EndPAD
$PAD
Sh "4" R 0.6 1.55 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1.5 1.5
.LocalClearance 0.07
$EndPAD
$PAD
Sh "5" R 0.6 1.55 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -0.5 1.5
.LocalClearance 0.07
$EndPAD
$PAD
Sh "6" R 0.6 1.55 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0.5 1.5
.LocalClearance 0.07
$EndPAD
$PAD
Sh "7" R 0.6 1.55 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1.5 1.5
.LocalClearance 0.07
$EndPAD
$PAD
Sh "8" R 0.6 1.55 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2.5 1.5
.LocalClearance 0.07
$EndPAD
$PAD
Sh "9" R 0.6 1.55 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3.5 1.5
.LocalClearance 0.07
$EndPAD
$PAD
Sh "10" R 0.6 1.55 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4.5 1.5
.LocalClearance 0.07
$EndPAD
$PAD
Sh "" R 0.8 1.8 0 0 0
Dr 0 0 0
At HOLE N 00E0FFFF
Ne 0 ""
Po 5.8 -1.025
$EndPAD
$EndMODULE T1M-10-V
$EndLIBRARY
