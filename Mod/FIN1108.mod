PCBNEW-LibModule-V1  Wed 14 Jan 2015 08:06:26 AM EST
# encoding utf-8
Units mm
$INDEX
FIN1108
FIN1108_RevB
$EndINDEX
$MODULE FIN1108
Po 0 0 0 15 546E3CA0 00000000 ~~
Li FIN1108
Sc 0
AR 
Op 0 0 0
T0 0 -6.9 1 1 0 0.15 N V 21 N "FIN1108"
T1 0 5.4 1 1 0 0.15 N V 21 N "VAL**"
T2 -6.7 4.6 1 1 0 0.15 N V 21 N "1"
DS -6.25 3.05 -6.25 -3.05 0.15 21
DS 6.25 3.05 6.25 -3.05 0.15 21
$PAD
Sh "27" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4.75 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "26" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 5.25 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "25" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 5.75 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "28" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4.25 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "29" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3.75 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "30" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3.25 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "31" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2.75 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "32" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2.25 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "35" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0.75 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "34" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1.25 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "33" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1.75 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "16" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1.75 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "15" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1.25 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "14" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0.75 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "17" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2.25 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "18" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2.75 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "19" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3.25 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "20" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3.75 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "21" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4.25 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "24" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 5.75 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "23" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 5.25 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "22" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4.75 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "9" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1.75 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "10" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1.25 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "11" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -0.75 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "8" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2.25 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "7" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2.75 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "6" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -3.25 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "5" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -3.75 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "4" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4.25 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "1" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -5.75 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "2" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -5.25 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "3" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4.75 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "46" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4.75 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "47" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -5.25 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "48" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -5.75 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "45" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4.25 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "44" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -3.75 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "43" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -3.25 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "42" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2.75 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "41" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2.25 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "36" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0.25 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "37" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -0.25 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "12" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -0.25 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "13" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0.25 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "38" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -0.75 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "39" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1.25 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "40" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1.75 -3.45
.LocalClearance 0.07
$EndPAD
$EndMODULE FIN1108
$MODULE FIN1108_RevB
Po 0 0 0 15 54B669CC 00000000 ~~
Li FIN1108_RevB
Sc 0
AR 
Op 0 0 0
T0 0 -6.9 1 1 0 0.15 N V 21 N "FIN1108"
T1 0 5.4 1 1 0 0.15 N V 21 N "VAL**"
DC -5.4 1.2 -5.1 1.6 0.15 21
T2 -6.7 4.6 1 1 0 0.15 N V 21 N "1"
DS -6.25 3.05 -6.25 -3.05 0.15 21
DS 6.25 3.05 6.25 -3.05 0.15 21
$PAD
Sh "27" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4.75 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "26" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 5.25 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "25" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 5.75 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "28" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4.25 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "29" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3.75 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "30" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3.25 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "31" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2.75 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "32" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2.25 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "35" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0.75 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "34" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1.25 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "33" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1.75 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "16" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1.75 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "15" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1.25 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "14" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0.75 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "17" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2.25 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "18" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2.75 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "19" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3.25 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "20" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3.75 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "21" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4.25 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "24" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 5.75 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "23" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 5.25 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "22" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4.75 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "9" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1.75 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "10" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1.25 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "11" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -0.75 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "8" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2.25 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "7" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2.75 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "6" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -3.25 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "5" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -3.75 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "4" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4.25 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "1" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -5.75 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "2" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -5.25 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "3" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4.75 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "46" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4.75 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "47" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -5.25 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "48" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -5.75 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "45" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4.25 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "44" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -3.75 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "43" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -3.25 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "42" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2.75 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "41" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2.25 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "36" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0.25 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "37" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -0.25 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "12" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -0.25 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "13" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0.25 3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "38" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -0.75 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "39" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1.25 -3.45
.LocalClearance 0.07
$EndPAD
$PAD
Sh "40" R 0.3 2.3 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1.75 -3.45
.LocalClearance 0.07
$EndPAD
$EndMODULE FIN1108_RevB
$EndLIBRARY
