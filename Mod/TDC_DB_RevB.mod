PCBNEW-LibModule-V1  Fri 09 Jan 2015 02:28:20 PM EST
# encoding utf-8
Units mm
$INDEX
LSHM-130-02.5
LSHM-130-02.5_RevB
$EndINDEX
$MODULE LSHM-130-02.5
Po 0 0 0 15 54B02B21 00000000 ~~
Li LSHM-130-02.5
Sc 0
AR /5461E2FD/54434BB9
Op 0 0 0
T0 0 -4 1 1 0 0.15 N V 21 N "J10"
T1 0 5.75 1 1 0 0.15 N I 21 N "LSHM-130-02.5-F-DV-N_(TDC_INPUTS)"
DS -18.8 4.2 44.2 4.2 0.15 21
DS 44.2 4.2 44.2 -15.8 0.15 21
DS 44.2 -15.8 -18.8 -15.8 0.15 21
DS -18.8 -15.8 -18.8 4.2 0.15 21
T2 -8 -3.2 1 1 0 0.15 N V 21 N "1"
DS -9.85 2.75 -9.85 -2.25 0.15 21
DS 9.85 2.75 9.85 -2.25 0.15 21
$PAD
Sh "" C 1.45 1.45 0 0 0
Dr 1.45 0 0
At HOLE N 00E0FFFF
Ne 0 ""
Po -8.5 -0.6
$EndPAD
$PAD
Sh "14" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 2 "/LVDS_3/LVDS_N0"
Po -4.25 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "16" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 3 "/LVDS_3/LVDS_N1"
Po -3.75 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "18" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 4 "/LVDS_3/LVDS_N2"
Po -3.25 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "24" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 7 "/LVDS_3/LVDS_N5"
Po -1.75 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "22" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 6 "/LVDS_3/LVDS_N4"
Po -2.25 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "20" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 5 "/LVDS_3/LVDS_N3"
Po -2.75 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "8" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 34 "GND"
Po -5.75 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "10" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 34 "GND"
Po -5.25 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "12" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 34 "GND"
Po -4.75 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "6" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 34 "GND"
Po -6.25 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "4" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -6.75 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "2" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -7.25 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "26" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 8 "/LVDS_3/LVDS_N6"
Po -1.25 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "28" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 9 "/LVDS_3/LVDS_N7"
Po -0.75 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "30" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 18 "/LVDS_4/LVDS_N0"
Po -0.25 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "60" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 1 "+4V"
Po 7.25 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "58" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 1 "+4V"
Po 6.75 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "56" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 1 "+4V"
Po 6.25 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "32" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 008C8000
Ne 19 "/LVDS_4/LVDS_N1"
Po 0.25 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "34" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 20 "/LVDS_4/LVDS_N2"
Po 0.75 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "36" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 21 "/LVDS_4/LVDS_N3"
Po 1.25 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "42" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 24 "/LVDS_4/LVDS_N6"
Po 2.75 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "40" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 23 "/LVDS_4/LVDS_N5"
Po 2.25 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "38" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 22 "/LVDS_4/LVDS_N4"
Po 1.75 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "50" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4.75 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "52" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 5.25 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "54" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 1 "+4V"
Po 5.75 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "48" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 34 "GND"
Po 4.25 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "46" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 34 "GND"
Po 3.75 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "44" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 25 "/LVDS_4/LVDS_N7"
Po 3.25 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "43" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 33 "/LVDS_4/LVDS_P7"
Po 3.25 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "45" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3.75 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "47" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4.25 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "53" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 5.75 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "51" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 34 "GND"
Po 5.25 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "49" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 34 "GND"
Po 4.75 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "37" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 30 "/LVDS_4/LVDS_P4"
Po 1.75 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "39" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 31 "/LVDS_4/LVDS_P5"
Po 2.25 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "41" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 32 "/LVDS_4/LVDS_P6"
Po 2.75 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "35" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 29 "/LVDS_4/LVDS_P3"
Po 1.25 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "33" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 28 "/LVDS_4/LVDS_P2"
Po 0.75 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "31" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 008C8000
Ne 27 "/LVDS_4/LVDS_P1"
Po 0.25 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "55" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6.25 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "57" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6.75 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "59" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 7.25 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "29" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 26 "/LVDS_4/LVDS_P0"
Po -0.25 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "27" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 17 "/LVDS_3/LVDS_P7"
Po -0.75 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "25" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 16 "/LVDS_3/LVDS_P6"
Po -1.25 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "1" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -7.25 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "3" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -6.75 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "5" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 34 "GND"
Po -6.25 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "11" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 34 "GND"
Po -4.75 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "9" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 34 "GND"
Po -5.25 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "7" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 34 "GND"
Po -5.75 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "19" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 13 "/LVDS_3/LVDS_P3"
Po -2.75 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "21" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 14 "/LVDS_3/LVDS_P4"
Po -2.25 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "23" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 15 "/LVDS_3/LVDS_P5"
Po -1.75 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "17" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 12 "/LVDS_3/LVDS_P2"
Po -3.25 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "15" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 11 "/LVDS_3/LVDS_P1"
Po -3.75 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "13" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 10 "/LVDS_3/LVDS_P0"
Po -4.25 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "" C 1.45 1.45 0 0 0
Dr 1.45 0 0
At HOLE N 00E0FFFF
Ne 0 ""
Po 8.5 -0.6
$EndPAD
$EndMODULE LSHM-130-02.5
$MODULE LSHM-130-02.5_RevB
Po 0 0 0 15 54B02B21 00000000 ~~
Li LSHM-130-02.5_RevB
Sc 0
AR /5461E2FD/54434BB9
Op 0 0 0
T0 0 -4 1 1 0 0.15 N V 21 N "J10"
T1 0 5.75 1 1 0 0.15 N I 21 N "LSHM-130-02.5-F-DV-N_(TDC_INPUTS)"
DS -18.8 4.2 44.2 4.2 0.15 21
DS 44.2 4.2 44.2 -15.8 0.15 21
DS 44.2 -15.8 -18.8 -15.8 0.15 21
DS -18.8 -15.8 -18.8 4.2 0.15 21
T2 -8 -3.2 1 1 0 0.15 N V 21 N "1"
DS -9.85 2.75 -9.85 -2.25 0.15 21
DS 9.85 2.75 9.85 -2.25 0.15 21
$PAD
Sh "" C 1.45 1.45 0 0 0
Dr 1.45 0 0
At HOLE N 00E0FFFF
Ne 0 ""
Po -8.5 -0.6
$EndPAD
$PAD
Sh "14" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 2 "/LVDS_3/LVDS_N0"
Po -4.25 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "16" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 3 "/LVDS_3/LVDS_N1"
Po -3.75 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "18" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 4 "/LVDS_3/LVDS_N2"
Po -3.25 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "24" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 7 "/LVDS_3/LVDS_N5"
Po -1.75 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "22" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 6 "/LVDS_3/LVDS_N4"
Po -2.25 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "20" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 5 "/LVDS_3/LVDS_N3"
Po -2.75 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "8" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 34 "GND"
Po -5.75 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "10" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 34 "GND"
Po -5.25 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "12" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 34 "GND"
Po -4.75 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "6" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 34 "GND"
Po -6.25 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "4" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -6.75 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "2" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -7.25 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "26" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 8 "/LVDS_3/LVDS_N6"
Po -1.25 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "28" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 9 "/LVDS_3/LVDS_N7"
Po -0.75 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "30" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 18 "/LVDS_4/LVDS_N0"
Po -0.25 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "60" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 1 "+4V"
Po 7.25 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "58" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 1 "+4V"
Po 6.75 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "56" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 1 "+4V"
Po 6.25 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "32" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 008C8000
Ne 19 "/LVDS_4/LVDS_N1"
Po 0.25 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "34" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 20 "/LVDS_4/LVDS_N2"
Po 0.75 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "36" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 21 "/LVDS_4/LVDS_N3"
Po 1.25 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "42" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 24 "/LVDS_4/LVDS_N6"
Po 2.75 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "40" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 23 "/LVDS_4/LVDS_N5"
Po 2.25 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "38" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 22 "/LVDS_4/LVDS_N4"
Po 1.75 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "50" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4.75 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "52" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 5.25 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "54" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 1 "+4V"
Po 5.75 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "48" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 34 "GND"
Po 4.25 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "46" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 34 "GND"
Po 3.75 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "44" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 25 "/LVDS_4/LVDS_N7"
Po 3.25 2.1
.LocalClearance 0.07
$EndPAD
$PAD
Sh "43" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 33 "/LVDS_4/LVDS_P7"
Po 3.25 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "45" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3.75 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "47" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 4.25 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "53" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 5.75 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "51" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 34 "GND"
Po 5.25 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "49" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 34 "GND"
Po 4.75 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "37" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 30 "/LVDS_4/LVDS_P4"
Po 1.75 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "39" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 31 "/LVDS_4/LVDS_P5"
Po 2.25 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "41" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 32 "/LVDS_4/LVDS_P6"
Po 2.75 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "35" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 29 "/LVDS_4/LVDS_P3"
Po 1.25 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "33" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 28 "/LVDS_4/LVDS_P2"
Po 0.75 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "31" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 008C8000
Ne 27 "/LVDS_4/LVDS_P1"
Po 0.25 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "55" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6.25 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "57" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6.75 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "59" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 7.25 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "29" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 26 "/LVDS_4/LVDS_P0"
Po -0.25 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "27" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 17 "/LVDS_3/LVDS_P7"
Po -0.75 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "25" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 16 "/LVDS_3/LVDS_P6"
Po -1.25 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "1" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -7.25 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "3" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -6.75 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "5" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 34 "GND"
Po -6.25 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "11" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 34 "GND"
Po -4.75 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "9" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 34 "GND"
Po -5.25 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "7" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 34 "GND"
Po -5.75 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "19" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 13 "/LVDS_3/LVDS_P3"
Po -2.75 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "21" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 14 "/LVDS_3/LVDS_P4"
Po -2.25 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "23" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 15 "/LVDS_3/LVDS_P5"
Po -1.75 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "17" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 12 "/LVDS_3/LVDS_P2"
Po -3.25 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "15" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 11 "/LVDS_3/LVDS_P1"
Po -3.75 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "13" R 0.3 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 10 "/LVDS_3/LVDS_P0"
Po -4.25 -1.6
.LocalClearance 0.07
$EndPAD
$PAD
Sh "" C 1.45 1.45 0 0 0
Dr 1.45 0 0
At HOLE N 00E0FFFF
Ne 0 ""
Po 8.5 -0.6
$EndPAD
$EndMODULE LSHM-130-02.5_RevB
$EndLIBRARY
